# Menjadi 44

Terima kasih atas ucapan dari ibu, bapak, rekan-rekan sekalian. Doa terbaik terkirim untuk ibu dan bapak sekalian. Semoga kita selalu di bawah lindungan-Nya.

![Logo Over 40 Strava Runners Club](https://dgalywyr863hv.cloudfront.net/pictures/clubs/338126/8055017/1/large.jpg)

[Logo Over 40 Strava Runners Club](https://dgalywyr863hv.cloudfront.net/pictures/clubs/338126/8055017/1/large.jpg) 

Artikel ini juga diarsipkan di [Gitlab/derwinirawan](https://gitlab.com/derwinirawan/menjadi44).

Ya, kemarin, 17 April 2020 saya memasuki usia 44 tahun. Banyak yang bilang usia hanya angka, usia 40 an masih muda, hidup dimulai usia 40, dst. Terima kasih atas semangatnya. Saya melihatnya agak lain. Usia 40 an ini adalah usia senja, karena usia bukan di tangan kita. Semua orang ingin berumur panjang, termasuk saya, sehingga jadi terlena untuk memberikan nilai lebih kepada kehidupan itu sendiri.

> Bagaimana dengan anda?

Saya berada di lingkungan akademik, saya seorang dosen geologi dengan fokus di bidang hidrogeologi. Dulu ayah saya bilang, sudah bagus kamu pilih profesi sebagai dosen. Profesi yang memungkin seseorang beramal jariah tanpa terasa telah melakukan itu. Waktu itu saya ingin menyalahkannya, tapi saya memilih untuk diam saja. Saya ingin bilang kalau yang beliau sebutkan itu salah besar. Ekstrimnya, tidak ada sedikitpun kegiatan yang dilakukan oleh seorang dosen yang ditempatkan sebagai amal jariah. Buktinya mudah, mayoritas dosen masih menerbitkan karyanya dengan menghalangi orang lain untuk menggunakannya dengan bebas, mayoritas masih menghitung jumlah sitasi, memilih-milih jurnal top dan mayoritas lainnya masih-masih tidak merasa perlu untuk menyebarkan ilmunya, takut dicuri katanya. Sebagian lainnya sibuk mengurus sertifikat pengakuan Hak Cipta bahkan paten.   

> Apakah anda termasuk di dalamnya? 

Selema beberapa tahun ini, selain melaksanakan tugas saya mengajar, meneliti tentang hidrogeologi, saya juga "terperangkap" pada pergaulan yang "tidak normal". Sebuah pergaulan dengan orang-orang yang berpikir bahwa ilmu pengetahuan adalah milik publik. Bahwa ilmu pengetahuan tidak seharusnya dibatasi oleh dinding-dinding yang tidak perlu. Orang-orang ini, menurut saya, juga masih diposisikan sebagai orang aneh di lingkungannya masing-masing. 

> Seperti juga saya. 

Di saat semua orang bergerak ke satu arah, kami bergerak ke arah yang berlawanan. Di saat tidak saling berhubunganpun, pikiran kami sama. Ingin ke satu arah yang berlawanan, tapi arah yang seharusnya di ambil para dosen atau peneliti, terutama yang bekerja untuk institusi publik dan digaji dengan uang publik.

Sejalan dengan waktu, saya juga makin tahu kalau dunia akademik bukanlah dunia yang terbuka seperti yang dibilang banyak orang, termasuk ayah saya. Ternyata pikiran warga dunia itu terperangkap dalam waktu. Suatu waktu di saat orang pintar tidak banyak, media untuk mengumumkan eksperimen hanya beberapa gelintir saja, dan teknologi percetakan adalah hanya kertas dan pena. Kalaupun berpikiran terbuka, orang-orang itu akan menerima pendapat baru untuk kemudian dibawa masuk ke dalam kerangka waktu zaman dulu. Kriteria-kriteria yang dipakainyapun adalah kriteria zaman dulu.

> Akibatnya semua hal baru akan tertolak

Satu lainnya adalah bahwa pandangan zaman dinosaurus itu diturunkan, dari staf senior kepada staf yunior. Persepsi tentang prestise dan gengsi dijejalkan ke dalam pemikiran staf muda dengan bungkus untuk peningkatan peringkat bangsa. Peringkat ya bukan kemajuan. Ketersediaan data, kejelasan metode, bukan lagi hal yang penting untuk diangkat. Akibatnya data yang terbuka dan mudah untuk digunakan ulang, nyaris tidak ada, seperti halnya dinosaurus. Metode yang terbukapun jarang dibuka lengkap, sehingga sulit untuk diperiksa ulang. Lebih parah lagi, pembaca tidak merasa perlu lagi memeriksa apa yang dibacanya setelah melihat label jurnal di mana makalah itu terbit. 

> Apakah anda termasuk yang menurunkan atau yang menerima turunan pemikiran itu?   

Melihat itu semua, saya tidak patah semangat. Setidaknya ini akan lebih memotivasi saya untuk melakukan lebih jauh lagi untuk membagikan ilmu pengetahuan yang tidak seberapa. Bukan hanya hasil yang saya dan rekan-rekan di kelompok saya saja, tetapi juga membagikan ilmu pengetahuan yang dihasilkan oleh kelompok lainnya. 

Apakah anda berpikiran sama? 

Salah satu yang akan saya lakukan adalah memaksimumkan karya dengan lisensi CC-0, terutama yang dibuat dengan sumberdaya saya sendiri. Saya kemudian menemukan [disertasi ini](https://thesiscommons.org/4wtpc/) yang ditulis oleh rekan Twitter saya [Chris Hartgerink](https://twitter.com/chartgerink). Usianya masih sangat muda sebagai seorang doktor, tapi pikirannya sudah melebihi seorang Guru Besar. 

Saya yakin ada rekan-rekan pembaca yang seperti ini juga. Saatnya anda semua memaksimumkan perangkat daring yang ada sekarang untuk meningkatkan profil akademik anda.    

